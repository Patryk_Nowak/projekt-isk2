﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class PROTECTED_AdminLayer_Konta : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            daneStudenta();
            daneWykladowcy();
        }
            DodawanieStudenta();
            DodawanieWykladowcy();
    }
    protected void daneStudenta()
    {

        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
            String.Format("select id, nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, idkierunku, mail, haslo from Studenci");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        GridView1.Visible = true;
        connection.Close();

    }
    protected void daneWykladowcy()
    {

        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
            String.Format("select id, imie, nazwisko, katedra, email, login, haslo from Wykladowcy");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView2.DataSource = dt;
        GridView2.DataBind();
        GridView2.Visible = true;
        connection.Close();

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //kasowanie po id, musialem dodać do Gridview DataKeyNames="Id"
        String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("delete from Studenci where id ={0}", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneStudenta();
    }

    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //kasowanie po id, musialem dodać do Gridview DataKeyNames="Id"
        String id = GridView2.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        //command.CommandText = String.Format("delete from Wykladowcy join PrzedmiotWykladowca where Wykladowcy.id = PrzedmiotWykladowca.idwykladowcy={0}", id);
        command.CommandText = String.Format("delete from PrzedmiotWykladowca where idwykladowcy={0};delete from Wykladowcy where id ={0}", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneWykladowcy();

    }

    //jak anulujesz edytowanie to wraca do domyślnej wartości indeksu dla Gridview
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        daneStudenta();
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        daneWykladowcy();
    }

    //to jest potrzebne do edycji
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        daneStudenta();
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        daneWykladowcy();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView1.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView1.Rows[e.RowIndex].Cells[i];
            GridView1.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        //jeden sposób na edycję
        command.CommandText = string.Format("update Studenci set nr_indeksu='{0}', imie='{1}', nazwisko='{2}', adres='{3}', data_urodzenia='{4}', pesel='{5}', data_rozpoczecia='{6}', aktualny_sem='{7}', tryb_studiow='{8}', kierunek='{9}', mail='{10}', haslo='{11}' where Id='{12}'", e.NewValues["nr_indeksu"], e.NewValues["imie"], e.NewValues["nazwisko"], e.NewValues["adres"], e.NewValues["data_urodzenia"], e.NewValues["pesel"], e.NewValues["data_rozpoczecia"], e.NewValues["aktualny_sem"], e.NewValues["tryb_studiow"], e.NewValues["kierunek"], e.NewValues["mail"], e.NewValues["haslo"], id);
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        GridView1.EditIndex = -1;
        daneStudenta();
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView2.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView2.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView2.Rows[e.RowIndex].Cells[i];
            GridView2.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }

        //drugi sposób na edycję
        command.CommandText = string.Format("update Wykladowcy set imie=@imie, nazwisko=@nazwisko, katedra=@katedra, email=@email, haslo=@haslo, login=@login where Id=@Id");
        command.CommandType = CommandType.Text;
        command.Parameters.AddWithValue("@imie", e.NewValues["imie"]);
        command.Parameters.AddWithValue("@nazwisko", e.NewValues["nazwisko"]);
        command.Parameters.AddWithValue("@katedra", e.NewValues["katedra"]);
        command.Parameters.AddWithValue("@email", e.NewValues["email"]);
        command.Parameters.AddWithValue("@haslo", e.NewValues["haslo"]);
        command.Parameters.AddWithValue("@login", e.NewValues["login"]);
        command.Parameters.AddWithValue("@Id", id);
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        GridView2.EditIndex = -1;
        daneWykladowcy();
    }
    public void DodawanieStudenta()
    {
        TextBox tbNumerIndeksu = new TextBox();
        TextBox tbImie = new TextBox();
        TextBox tbNazwisko = new TextBox();
        TextBox tbAdres = new TextBox();
        TextBox tbDataUrodzenia = new TextBox();
        TextBox tbPesel = new TextBox();
        TextBox tbDataRozpoczecia = new TextBox();
        TextBox tbAktualnySemestr = new TextBox();
        TextBox tbTryb = new TextBox();
        TextBox tbKierunek = new TextBox();
        TextBox tbEmail = new TextBox();
        Button btDodaj = new Button() { Text = "Dodaj" };
        btDodaj.Click += new EventHandler(DodajStudenta);
        GridView1.FooterRow.Cells[2].Controls.Add(tbNumerIndeksu);
        GridView1.FooterRow.Cells[3].Controls.Add(tbImie);
        GridView1.FooterRow.Cells[4].Controls.Add(tbNazwisko);
        GridView1.FooterRow.Cells[5].Controls.Add(tbAdres);
        GridView1.FooterRow.Cells[6].Controls.Add(tbDataUrodzenia);
        GridView1.FooterRow.Cells[7].Controls.Add(tbPesel);
        GridView1.FooterRow.Cells[8].Controls.Add(tbDataRozpoczecia);
        GridView1.FooterRow.Cells[9].Controls.Add(tbAktualnySemestr);
        GridView1.FooterRow.Cells[10].Controls.Add(tbTryb);
        GridView1.FooterRow.Cells[11].Controls.Add(tbKierunek);
        GridView1.FooterRow.Cells[12].Controls.Add(tbEmail);
        GridView1.FooterRow.Cells[14].Controls.Add(btDodaj);
    }

    public void DodawanieWykladowcy()
    {
        TextBox tbImie = new TextBox();
        TextBox tbNazwisko = new TextBox();
        TextBox tbKatedra = new TextBox();
        TextBox tbEmail = new TextBox();
        TextBox tbLogin = new TextBox();
        Button btDodaj = new Button() { Text = "Dodaj" };
        btDodaj.Click += new EventHandler(DodajWykladowce);
        GridView2.FooterRow.Cells[2].Controls.Add(tbImie);
        GridView2.FooterRow.Cells[3].Controls.Add(tbNazwisko);
        GridView2.FooterRow.Cells[4].Controls.Add(tbKatedra);
        GridView2.FooterRow.Cells[5].Controls.Add(tbEmail);
        GridView2.FooterRow.Cells[6].Controls.Add(tbLogin);
        GridView2.FooterRow.Cells[8].Controls.Add(btDodaj);
    }

    public void DodajStudenta(object sender, EventArgs e)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        String numerIndeksu = ((TextBox)GridView1.FooterRow.Cells[2].Controls[0]).Text;
        String imie = ((TextBox)GridView1.FooterRow.Cells[3].Controls[0]).Text;
        String nazwisko = ((TextBox)GridView1.FooterRow.Cells[4].Controls[0]).Text;
        String adres = ((TextBox)GridView1.FooterRow.Cells[5].Controls[0]).Text;
        String dataUrodzenia = ((TextBox)GridView1.FooterRow.Cells[6].Controls[0]).Text;
        String pesel = ((TextBox)GridView1.FooterRow.Cells[7].Controls[0]).Text;
        String dataRozpoczecia = ((TextBox)GridView1.FooterRow.Cells[8].Controls[0]).Text;
        String aktualnySemestrText = ((TextBox)GridView1.FooterRow.Cells[9].Controls[0]).Text;
        int aktualnySemestr;
        if (!int.TryParse(aktualnySemestrText, out aktualnySemestr))
        {
            aktualnySemestr = 0;
        }
        String tryb = ((TextBox)GridView1.FooterRow.Cells[10].Controls[0]).Text;
        String kierunek = ((TextBox)GridView1.FooterRow.Cells[11].Controls[0]).Text;
        String email = ((TextBox)GridView1.FooterRow.Cells[12].Controls[0]).Text;
        String haslo = Haslo();
        command.CommandText = String.Format(
            "insert into Studenci(nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, idkierunku, mail, haslo) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')",
            numerIndeksu, imie, nazwisko, adres, dataUrodzenia, pesel, dataRozpoczecia, aktualnySemestr, tryb, kierunek, email, haslo);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneStudenta();
        //WyslijPotwierdzenie(email, numerIndeksu, haslo);
    }
    public void DodajWykladowce(object sender, EventArgs e)
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        String imie = ((TextBox)GridView2.FooterRow.Cells[2].Controls[0]).Text;
        String nazwisko = ((TextBox)GridView2.FooterRow.Cells[3].Controls[0]).Text;
        String katedra = ((TextBox)GridView2.FooterRow.Cells[4].Controls[0]).Text;
        String email = ((TextBox)GridView2.FooterRow.Cells[5].Controls[0]).Text;
        String login = ((TextBox)GridView2.FooterRow.Cells[6].Controls[0]).Text;
        String haslo = Haslo();
        command.CommandText = String.Format(
    "insert into Wykladowcy(imie, nazwisko, katedra, email, haslo, login) values('{0}','{1}','{2}','{3}','{4}','{5}')",
     imie, nazwisko, katedra, email, haslo, login);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneWykladowcy();
        //WyslijPotwierdzenie(email, login, haslo);
    }

    public void WyslijPotwierdzenie(string email, string login, string haslo)
    {
        string tresc = string.Format("Witamy w Wirtualnym Dziekanacie, została zakończona pomyślnie. Twoje dane logowania to login: {0}, hasło: {1} . Serdecznie pozdrawiamy, Dziekanat!", login, haslo);


        SmtpClient client = new SmtpClient();
        client.Port = 587;
        client.Host = "smtp.gmail.com";
        client.EnableSsl = true;
        client.Timeout = 10000;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.Credentials = new System.Net.NetworkCredential("wirtualnydziekanatisk2@gmail.com", "Qwerty_123");
        MailMessage mm = new MailMessage("wirtualnydziekanatisk2@gmail.com", email, "Wirtualny Dziekanat potwierdzenie", tresc);
        mm.BodyEncoding = System.Text.UTF8Encoding.UTF8;
        //mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        client.Send(mm);
    }
    public string Haslo()
    {
        Random random = new Random();
        string haslo = "";
        for (int i = 0; i < 6; i++)
        {
            if (i % 2 == 0)
            {
                haslo += (char)random.Next('a', 'z');
            }
            else
            {
                haslo += random.Next(10);
            }
        }
        return haslo;
    }


}
